﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[System.Serializable]
public class Chromosome 
{
    public static Dictionary<char, Vector3> cellPositions = new Dictionary<char, Vector3>() {
        {'A', new Vector3 (0.0f, 0.0f, 1.0f)},
        {'C', new Vector3 (0.0f, 0.0f, -1.0f)},
        {'G', new Vector3 (0.0f, 1.0f, 0.0f)},
        {'T', new Vector3 (0.0f, -1.0f, 0.0f)},
        {'X', new Vector3 (1.0f, 0.0f, 0.0f)},
        {'Y', new Vector3 (-1.0f, 0.0f, 0.0f)}
    };
    public string sequence;
    public Vector3 relativeTorque;

    public Chromosome() {}

    public Chromosome(Chromosome original)
    {
        sequence = original.sequence;
        relativeTorque = original.relativeTorque;
    }

    public static Chromosome CreateRandom()
    {
        return new Chromosome
        {
            sequence = GenerateRandomSequence(),
            relativeTorque = new Vector3().Randomize(-10, 10),
        };
    }

    private static string GenerateRandomSequence()
    {
        string newSequence = "";
        int nrOfCellsToSpawn = Random.Range(0, cellPositions.Count);
        for (int i = 0; i <= nrOfCellsToSpawn; ++i)
        {
            newSequence += cellPositions.Keys.ElementAt(Random.Range(0, cellPositions.Count));
        }
        return newSequence;
    }

    public static Chromosome Splice(Chromosome parentA, Chromosome parentB)
    {
        Chromosome child = new Chromosome();
        child.relativeTorque = Vector3.Lerp(parentA.relativeTorque, parentB.relativeTorque, 0.5f);

        char[] seqA = parentA.sequence.ToCharArray();
        char[] seqB = parentB.sequence.ToCharArray();
        char[] seqC = seqB.Length > seqA.Length ? seqA : seqB;
        
        for (int i = 0; i < seqC.Length; ++i)
        {
            if (Random.value < 0.5f)
            {
                seqC[i] = seqA[i];
            }
            else
            {
                seqC[i] = seqB[i];
            }
        }
        
        child.sequence = new string(seqC);

        return child;
    }

    public void Mutate()
    {
        relativeTorque = new Vector3().Randomize(-10, 10);
        for (int i = 0; i < Random.Range(0, 4); ++ i)
        {
            int insertAt = Random.Range(0, sequence.Length);
            char label = cellPositions.Keys.ElementAt(Random.Range(0, cellPositions.Count));
            sequence = sequence.Substring(0, insertAt) + label + sequence.Substring(insertAt);
        }
    }
};

