﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EvolutionManager : MonoBehaviour
{
    public GameObject OrganismPrefab;
    public int PopulationSize = 10;
    public int GenerationCount = 0;
    public float LifeSpan = 20;
    [Range (0, 30)]
    public float TimeScale = 1;

    private List<GameObject> population;
    private float timeOfLastCull = 0;

    void Start()
    {
        population = new List<GameObject>();
        for (int i = 0; i < PopulationSize; ++i)
        {
            var org = Instantiate(OrganismPrefab, 
                            new Vector3(i * 10, 0, 0), 
                            Quaternion.identity);
            org.name = "Organism " + i;
            org.GetComponent<Organism>().AssignChromosome(Chromosome.CreateRandom());
            population.Add(org);
        }
    }

    void Update()
    {
        if (Time.time - timeOfLastCull > LifeSpan)
        {
            NextGeneration();
            timeOfLastCull = Time.time;
        }

        Time.timeScale = TimeScale;
    }

    private void NextGeneration()
    {
        SortPopulationByScore();
        var topChromosomes = population
                                    .Take((int) (PopulationSize * 0.1))
                                    .Select(x => x.GetComponent<Organism>().chromosome)
                                    .ToList();
        
        KillCurrentPopulation();
        SpawnNewPopulation(topChromosomes);

        ++GenerationCount;
    }

    private void SortPopulationByScore()
    {
        population.Sort((org1, org2)=>
            Score(org2).CompareTo(Score(org1))
        );
    }

    private float Score(GameObject anOrganism)
    {
        float score = 0;
        Vector3 pos = anOrganism.transform.position;
        score += pos.z;
        score += (pos.y * 10);
        return score;
    }

    private void KillCurrentPopulation()
    {
        for (int i = 0; i < population.Count; ++i)
        {
            Destroy(population[i]);
        }
        population.Clear();
    }

    private void SpawnNewPopulation(List<Chromosome> chromosomes)
    {
        for (int i = 0; i < PopulationSize; ++i)
        {
            var org = Instantiate(OrganismPrefab, 
                            new Vector3(i * 10, 0, 0), 
                            Quaternion.identity);
            org.name = "Organism " + i;
            Chromosome parentA = chromosomes.RandomElement();
            Chromosome parentB = chromosomes.RandomElement();
            Chromosome newChromosome = Chromosome.Splice(parentA, parentB);
                        
            if (Random.value < 0.2f)
            {
                newChromosome.Mutate();
            }
            org.GetComponent<Organism>()
                    .AssignChromosome(newChromosome);
            
            population.Add(org);
        }
    }
}
