﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExtensionMethods
{
    public static Vector3 Randomize(this Vector3 vec, float min, float max)
    {
        vec.x = Random.Range(min, max);
        vec.y = Random.Range(min, max);
        vec.z = Random.Range(min, max);
        return vec;
    }

    public static T RandomElement<T>(this List<T> items)
    {
        return items[Random.Range(0, items.Count)];
    }
}
