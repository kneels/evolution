﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ArrayEditor : EditorWindow
{	
	private int count;
	private Vector3 posOffset = new Vector3(1f, 0f, 0f);
	private Vector3 rotOffset = new Vector3(0f, 0f, 0f);
	private Vector3 scaleOffset = new Vector3(0f, 0f, 0f);
	private Dictionary<GameObject, List<GameObject>> originalsAndClones = new Dictionary<GameObject, List<GameObject>>();
	private GameObject[] selection = null;
	private Vector2 scrollPos;

	[MenuItem("Window/Custom/Array Editor")]
	public static void ShowWindow()
	{			
		EditorWindow.GetWindow(typeof(ArrayEditor));
	}

	void Update()
	{
		if (count < 0 || null == selection)
		{
			return;
		}

		UpdateClones();
	}

	void OnGUI()
	{
		EditorGUI.BeginDisabledGroup (null == selection || selection.Length == 0);
		GUILayout.Label ("Settings", EditorStyles.boldLabel);
		count = Mathf.Max(EditorGUILayout.IntField("Count", count), 0);

		posOffset = EditorGUILayout.Vector3Field("Relative position offset", posOffset);
		rotOffset = EditorGUILayout.Vector3Field("Relative rotation offset", rotOffset);
		scaleOffset = EditorGUILayout.Vector3Field("Relative scale offset", scaleOffset);
		EditorGUI.EndDisabledGroup();

		EditorGUILayout.BeginHorizontal();
		if(GUILayout.Button("Select", GUILayout.Width(120)))
		{			
			selection = Selection.gameObjects;
			Reset();
		}
		else if(GUILayout.Button("Finish", GUILayout.Width(120)))
		{
			selection = null;
			Reset();
		}
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.LabelField("Selection", EditorStyles.boldLabel);
		scrollPos = EditorGUILayout.BeginScrollView(scrollPos, GUILayout.Width(256), GUILayout.Height(128));
		if (null != selection)
		{
			foreach (GameObject go in selection)
			{
				EditorGUILayout.LabelField("- " + go.name);
			}			
		}
		EditorGUILayout.EndScrollView();
	}

	void UpdateClones()
	{
		if (null == selection) 
		{
			return;
		}

		foreach (GameObject go in selection)
		{
			List<GameObject> clones; // the clones for this original
			originalsAndClones.TryGetValue(go, out clones);
			if (null == clones)
			{
				// Original wasn't present yet
				clones = new List<GameObject>();
				originalsAndClones.Add(go, clones);
			}
			
			TrimPadArray(go, clones, count);
			
			// update positions and rotations
			Transform previous = go.transform;
			Quaternion rot = previous.rotation;
			Vector3 pos = previous.TransformPoint(posOffset);
			Vector3 scale = previous.localScale;

			for (int i = 0; i < clones.Count; ++i)
			{
				if (null == clones[i])
				{
					// User manually deleted clone
					--count;
					GameObject.DestroyImmediate(clones[i]);
					clones.RemoveAt(i);
					continue;
				}
				clones[i].transform.rotation = rot * Quaternion.Euler(rotOffset);
				clones[i].transform.position = pos;
				clones[i].transform.localScale = scale - scaleOffset;

				previous = clones[i].transform;
				pos = previous.TransformPoint(posOffset);
				rot = previous.transform.rotation;
				scale = previous.localScale;
			}
		}		
	}

	void TrimPadArray(GameObject original, List<GameObject> clones, int newSize)
	{
		if (newSize > clones.Count)
		{
			// pad
			GameObject toClone = original;
			string name = original.name + " (Clone)";

			for (int i = clones.Count; i < newSize; ++i)
			{
				var clone = Instantiate(toClone);
				Undo.RegisterCreatedObjectUndo(clone, "Instantiated " + clone.name);
				clone.transform.parent = original.transform.parent;

				originalsAndClones[original].Add(clone);
				clone.name = name;

			}
		}
		else if (newSize < clones.Count)
		{
			// trim
			for (int i = clones.Count - 1; i >= newSize; --i)
			{
				GameObject.DestroyImmediate(clones[i]);
				clones.RemoveAt(i);
			}
		}
	}

	private void Reset()
	{
		originalsAndClones.Clear();
		count = 0;
		posOffset = new Vector3(1f, 0f, 0f);
		rotOffset = Vector3.zero;
		scaleOffset = Vector3.zero;		
	}
}