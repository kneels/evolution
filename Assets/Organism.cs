﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;



[RequireComponent (typeof(Rigidbody))]
public class Organism : MonoBehaviour
{
    private Rigidbody rigidBody;
    public Chromosome chromosome;

    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        SpawnRootNode();
    }

    public void AssignChromosome(Chromosome someChromosome)
    {
        this.chromosome = someChromosome;
        Build(gameObject, chromosome.sequence);
    }

    void FixedUpdate()
    {
        rigidBody.AddTorque(chromosome.relativeTorque);
    }

    void SpawnRootNode()
    {
        var go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        go.transform.parent = this.transform;
        go.transform.localPosition = Vector3.zero;
        go.name = "RootCell";
    }

    public static void Build(GameObject parentCell, string chromosome)
    {
        var charArr = chromosome.ToCharArray();
        GameObject lastCell = parentCell;
        for (int i = 0; i < charArr.Length; ++i)
        {
            char c = charArr[i];
            if (Chromosome.cellPositions.ContainsKey(c))
            {
                Vector3 pos = Chromosome.cellPositions[charArr[i]];
                var childCell = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                childCell.transform.parent = parentCell.transform;
                childCell.transform.localPosition = pos;
                childCell.name = "Cell " + charArr[i];
                lastCell = childCell;
            }
            else if (c == '(')
            {
                Build(lastCell, chromosome.Substring(i + 1));
                return;
            }
            else if (c == ')')
            {
                return;
            }
        }
    }

}
